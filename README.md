## [Demo](https://sailplay-filter.now.sh/)

## Request params

* `name` (`?name=Adeline%20Ortiz`)
* `date_from`, `date_to` (`?date_from=2015-02-01&date_to=2018-02-01`)
* `earned_from`, `earned_to`
* `spent_from`, `spent_to`

## Development

### Local

```sh
yarn install
yarn start
yarn run webpack
```

### Build for production

```sh
yarn run build
yarn run webpack:production
```